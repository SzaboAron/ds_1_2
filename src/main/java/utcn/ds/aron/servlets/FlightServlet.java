package utcn.ds.aron.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import utcn.ds.aron.dao.CityDAO;
import utcn.ds.aron.dao.FlightDAO;
import utcn.ds.aron.entities.Flight;

/**
 * Servlet implementation class FlightServlet
 */
public class FlightServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDao;
	private CityDAO cityDao; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlightServlet() {
        super();
        SessionFactory sessionFactory;
        try {
        	//new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml")
            sessionFactory = new Configuration().configure(new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml"))
                    .buildSessionFactory();
            flightDao = new FlightDAO(sessionFactory);
            cityDao = new CityDAO(sessionFactory);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		List<Flight> flights = flightDao.findAll();
		String style = "<style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;}</style>";
		String header = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Admin page</title></head>"+style+"<body>";
		String tableheader = "<table><tr><th>Flight ID</th><th>Airplane Type</th><th>Departure City</th>><th>Departure Time</th>><th>Arrival City</th>><th>Arrival Time</th></tr>";
		out.print(header);
		out.print(tableheader);
		
		for (Flight temp:flights){
			String depCity = cityDao.findById(temp.getDepcityid()).getName();
			String arrCity = cityDao.findById(temp.getArrivcityid()).getName();
			String s = "<tr>";
			s+="<td>"+temp.getId()+"</td><td>"+temp.getType()+"</td><td>"+depCity+"</td><td>"+temp.getDeptime()+"</td><td>"+arrCity+"</td><td>"+temp.getArrivtime()+"</td>";
			s+="</tr>";
			out.print(s);
		}
		
		out.print("</table></body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
