package utcn.ds.aron.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import utcn.ds.aron.dao.UserDao;
import utcn.ds.aron.entities.User;

/**
 * Servlet implementation class loginServlet
 */
public class loginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
        super();
        SessionFactory sessionFactory;
            try {
            	//new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml")
                sessionFactory = new Configuration().configure(new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml"))
                        .buildSessionFactory();
                userDao = new UserDao(sessionFactory);
            } catch (Throwable ex) {
                System.err.println("Initial SessionFactory creation failed." + ex);
                throw new ExceptionInInitializerError(ex);
            }
       
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In login GET");
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("user");
		if (user == null){
			request.getRequestDispatcher("login.html").forward(request, response);
			return;
		}
		System.out.println(user.toString());
		if (user.getAdmin().equals("admin"))
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
		else{
			if (user.getAdmin().equals("user")){
				request.getRequestDispatcher("/WEB-INF/userpage.html").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In login POST");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String userName = "";
		String password = "";
		userName = request.getParameter("userName");
		password = request.getParameter("password");
		User user = userDao.findUserByUserName(userName);
		if (user == null){
			out.println("User not found!");
			return;
		}else if( !user.getPassword().equals(password) ){
			out.println("Incorrect Password");
			return;
		}else{
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			//out.println("Successfull LOGIN "+ user.getUserName()+" with passw: "+user.getPassword()+" you are: "+user.getAdmin());
			if (user.getAdmin().equals("admin"))
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
			else{
				request.getRequestDispatcher("/WEB-INF/userpage.html").forward(request, response);
			}
		}	
	}

}
