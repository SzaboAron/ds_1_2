package utcn.ds.aron.servlets;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import utcn.ds.aron.dao.FlightDAO;
import utcn.ds.aron.entities.Flight;
import utcn.ds.aron.entities.User;

/**
 * Servlet implementation class adminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        SessionFactory sessionFactory;
        try {
        	//new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml")
            sessionFactory = new Configuration().configure(new File("/home/aron/Documents/Projects/workspace/ds_1_2/src/main/resources/hibernate.cfg.xml"))
                    .buildSessionFactory();
            flightDao = new FlightDAO(sessionFactory);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In Admin GET");
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("user");
		if (user == null){
			request.getRequestDispatcher("login.html").forward(request, response);
			System.out.println("User NULL!!");
			return;
		}
		System.out.println(user.toString());
		if (user.getAdmin().equals("admin"))
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
		else{
			if (user.getAdmin().equals("user")){
				request.getRequestDispatcher("/WEB-INF/userpage.html").forward(request, response);
			}else{
				request.getRequestDispatcher("login.html").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("user");
		if (user == null){
			request.getRequestDispatcher("login.html").forward(request, response);
			System.out.println("User NULL!!");
			return;
		}
		if (user.getAdmin().equals("user")){
				request.getRequestDispatcher("/WEB-INF/userpage.html").forward(request, response);
				return;
		}
		
		System.out.println("In Admin POST");
		Flight flight = new Flight();
		String idString = request.getParameter("flightid");
		if (idString == null){
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
			return;
		}
		int id = Integer.parseInt(idString);
		flight.setId(id);
		String type = request.getParameter("type");
		if (type == null || type.equals("")){
			flightDao.deleteFlight(id);
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
			return;
		}
		flight.setType(type);
		flight.setArrivcityid(Integer.parseInt(request.getParameter("arrivcity")));
		flight.setDepcityid(Integer.parseInt(request.getParameter("departcity")));
		String datestring = request.getParameter("departtime");		
		System.out.println(datestring);
		//LocalDateTime dateTime = LocalDateTime.parse(datestring, formatter);
		flight.setDeptime(datestring);
		datestring = request.getParameter("arrivtime");
		//dateTime = LocalDateTime.parse(datestring, formatter);
		flight.setArrivtime(datestring);
		//System.out.println(dateTime);
		Flight existing = flightDao.findById(flight.getId());
		if (existing == null){
			flightDao.addFlight(flight);
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
			return;
		}else{
			flightDao.updateFlight(flight);
			request.getRequestDispatcher("/WEB-INF/admin.html").forward(request, response);
		}
	}
	
	/*
	 * 
	 * Unfortunately Html does not support PUT and DELETE methods, thus I used
	 * the POST method to do the update and the delete tasks
	 * @see javax.servlet.http.HttpServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("This is the Delete Method!!!!");
	}

}
