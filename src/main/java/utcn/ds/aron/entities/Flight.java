package utcn.ds.aron.entities;

public class Flight {

	private int id;
	private String type;
	private int depcityid;
	private String deptime;
	private int arrivcityid;
	private String arrivtime;
	
	public Flight(){
		
	}
	
	public Flight(int id, String type, int depcityid, String deptime, int arrivcityid, String arrivtime) {
		super();
		this.id = id;
		this.type = type;
		this.depcityid = depcityid;
		this.deptime = deptime;
		this.arrivcityid = arrivcityid;
		this.arrivtime = arrivtime;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getDepcityid() {
		return depcityid;
	}
	public void setDepcityid(int depcityid) {
		this.depcityid = depcityid;
	}
	public String getDeptime() {
		return deptime;
	}
	public void setDeptime(String deptime) {
		this.deptime = deptime;
	}
	public int getArrivcityid() {
		return arrivcityid;
	}
	public void setArrivcityid(int arrivcityid) {
		this.arrivcityid = arrivcityid;
	}
	public String getArrivtime() {
		return arrivtime;
	}
	public void setArrivtime(String arrivtime) {
		this.arrivtime = arrivtime;
	}
	@Override
	public String toString() {
		return "Flight [id=" + id + ", type=" + type + ", depcityid=" + depcityid + ", deptime=" + deptime
				+ ", arrivcityid=" + arrivcityid + ", arrivtime=" + arrivtime + "]";
	}
	
	
	
}
