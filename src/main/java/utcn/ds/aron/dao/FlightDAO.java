package utcn.ds.aron.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import utcn.ds.aron.entities.Flight;

public class FlightDAO {

	private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);
	private SessionFactory factory;
	
	public FlightDAO(SessionFactory factory){
		this.factory = factory; 
	}

	public Flight addFlight(Flight flight){
		Session session = factory.openSession();
		Transaction tx = null;
		//int flightId = -1;
		try {
			tx = session.beginTransaction();
			session.save(flight);
			//flight.setId(flightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			System.out.println("Flight added!!");
			session.close();
		}
		return flight;
	}
	
	public Flight deleteFlight(int id){
		Session session = factory.openSession();
		Transaction tx = null;
		Flight flight;
		try {
			tx = session.beginTransaction();
			flight = session.load(Flight.class,id);
			if (flight == null) return null;
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			System.out.println("Flight added!!");
			session.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> findAll(){
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			//query.setParameter("flightname", flightName);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights;
	}
	
	@SuppressWarnings("unchecked")
	public Flight findById(int id){
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			//query.setParameter("flightname", flightName);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		if (flights == null) return null;
		if (flights.isEmpty()) return null;
		for (Flight temp:flights){
			if (temp.getId() == id) return temp;
		}
		return null;
	}
	
	public Flight updateFlight(Flight flight){
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			System.out.println("Flight Updated!!");
			session.close();
		}
		return flight;
	}
}
