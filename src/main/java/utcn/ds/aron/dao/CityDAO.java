package utcn.ds.aron.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import utcn.ds.aron.entities.City;

public class CityDAO {
	
	private static final Log LOGGER = LogFactory.getLog(CityDAO.class);
	private SessionFactory factory;
	
	public CityDAO(SessionFactory factory){
		this.factory = factory; 
	}

	public City addCity(City city){
		Session session = factory.openSession();
		Transaction tx = null;
		int cityId = -1;
		try {
			tx = session.beginTransaction();
			cityId = (Integer) session.save(city);
			city.setId(cityId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return city;
	}
	
	public City deleteCity(int id){
		//TODO
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public City findById(int id){
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> citys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City");
			citys = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		if (citys == null || citys.isEmpty()) return null;
		for (City temp:citys){
			if (temp.getId() == id) return temp;
		}
		return null;
	}
	
	public City updateCity(int id){
		//TODO
		return null;
	}
}
