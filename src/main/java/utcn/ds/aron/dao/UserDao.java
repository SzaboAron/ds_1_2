package utcn.ds.aron.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import utcn.ds.aron.entities.User;

public class UserDao {
	private static final Log LOGGER = LogFactory.getLog(UserDao.class);
	private SessionFactory factory;
	
	public UserDao(SessionFactory factory){
		this.factory = factory; 
	}

	public User addUser(User user){
		Session session = factory.openSession();
		Transaction tx = null;
		int userId = -1;
		try {
			tx = session.beginTransaction();
			userId = (Integer) session.save(user);
			user.setId(userId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return user;
	}
	
	public User deleteUser(int id){
		//TODO
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public User findUserByUserName(String userName){
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User");
			//query.setParameter("username", userName);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		if (users == null || users.isEmpty()) return null;
		for (User temp:users){
			if (temp.getUserName().equals(userName)) return temp;
		}
		return null;
	}
	
	public User updateUser(int id){
		//TODO
		return null;
	}
}
